package com.jiraload.page;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.jiraload.pagefactory.testbase;

public class ProjectDetail extends testbase {
	 
	WebDriver driver;
	
	public ProjectDetail(WebDriver driver) {		
	this.driver= driver;	
	}
	
	 public void selectProject()
	 {   wait= new WebDriverWait(driver, 2000);
		
		 	try {
				WebElement adminMenu  = wait.until(ExpectedConditions.elementToBeClickable(By.id("admin_menu")));
				adminMenu.click();
				driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);  
				WebElement projectmenu  = wait.until(ExpectedConditions.elementToBeClickable(By.id("admin_project_menu")));
				projectmenu.click();
				WebElement project = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//a[contains(text(),'M63_ProjectOne')]")));
				project.click();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 
	 }

}
