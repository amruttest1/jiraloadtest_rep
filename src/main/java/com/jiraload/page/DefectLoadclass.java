package com.jiraload.page;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import com.jiraload.pagefactory.*;
import com.jiraload.page.ProjectDetail;

public class DefectLoadclass extends  testbase{
	WebDriver driver;
	ProjectDetail PD ;  
	
	//Issue from the header menu 
//	@FindBy(xpath ="//*[@id='find_link']")
//	WebElement MenuIssueLink;
	
	// search link form issue dropdown
//	@FindBy(xpath ="//*[@id='issues_new_search_link_lnk']")
//	WebElement IssurSearchLink;
	
	//advance search button
//	@FindBy(xpath ="//*[@id='content']/div[1]/div[3]/div/form/div[1]/div[1]/div[1]/div[2]/div/a[1]")
//	WebElement AdvanceSearch;
	
//	@FindBy(xpath ="//*[@id='advanced-search']")
//	WebElement searchtxtbox;
	
//	@FindBy(xpath ="//*[@id='issuetable']/tbody/tr/td[2]/a")
//	WebElement firstissue;
//	
	public DefectLoadclass(WebDriver driver) {
		this.driver = driver;
	}
	
	public void defectload() throws FileNotFoundException, IOException, InterruptedException
	{
		//  select project 
		projectload();
		
		String Bugsearch = BugString();
		issueSearchAction();
				
		searchLink();
		
		// click on advance search button 
//		WebElement Advancesearch=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='content']/div[1]/div[3]/div/form/div[1]/div[1]/div[1]/div[2]/div/a[1]")));
//		Advancesearch.click();
		bugSearchString(Bugsearch);
		// click on first bug issue from the result.
		
		firstBug();
		//System.out.println(firstissue.getText());
			
	}

	private void searchLink() {
		// click on search issue link from the dropdown 
		//IssurSearchLink.click();
		WebElement IssueSearchLink=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='issues_new_search_link_lnk']")));
		IssueSearchLink.click();
	}
	
	@Test(priority = 6)
	private void firstBug() {
		WebElement firstissue=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='issuetable']/tbody/tr/td[2]/a")));
		System.out.println("Issue Key : "+firstissue.getAttribute("data-issue-key"));
		
		firstissue.click();
		
		System.out.println("Clicked ...");
	}
	@Test(priority = 5)
	private void bugSearchString(String Bugsearch) throws InterruptedException {
		// enter the search string 
		WebElement searchtxtbox=driver.findElement(By.xpath("//*[@id='advanced-search']"));		
		searchtxtbox.sendKeys(Bugsearch);
		searchtxtbox.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
	}

	@Test(priority = 4)
	private void issueSearchAction() {
		// clickon the issue from the header 
		WebElement MenuIssueLink=driver.findElement(By.xpath("//*[@id='find_link']"));
		MenuIssueLink.click();
		driver.manage().timeouts().implicitlyWait(1000,TimeUnit.SECONDS);
	}

	
	private String BugString() {
		// bug search string 
		String Bugsearch="project =M63_ProjectOne and issuetype = Bug";
		return Bugsearch;
	}
	@Test(priority = 3)
	private void projectload() {
		PD= new ProjectDetail(driver);
		PD.selectProject();
	}

}
