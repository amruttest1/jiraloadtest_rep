package com.jiraload.page;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.jiraload.pagefactory.testbase;

public class ViewBoard extends testbase {
	WebDriver driver; 
	
	By MenuBoardLink= By.xpath("//*[@id='greenhopper_menu']");	
	By ViewAllBoardLink= By.xpath("//*[@id='ghx-manageviews-mlink_lnk']");
	By SearchTxtBox= By.xpath("//*[@placeholder='Find a board']");
	
	
	public ViewBoard(WebDriver driver) {
		this.driver=driver;
	}
	
	public void projectBoard() throws FileNotFoundException, IOException
	{  // select board from the admin menu bar
		// select on view all board 
		// enter the board name in search text box
		// select the board from the search result 
		
		try {
			driver.findElement(MenuBoardLink).click();
			driver.findElement(ViewAllBoardLink).click();
			driver.findElement(SearchTxtBox).sendKeys(getdata("boardName"),Keys.TAB);
			driver.findElement(By.xpath("//*[@title='Go to this board'][contains(text(),'"+getdata("boardName")+"')]")).click();
			
			driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
			
			Assert.assertTrue(driver.findElement(By.xpath("//*[@id='ghx-board-name'][contains(text(),'"+getdata("boardName")+"')]")).isDisplayed(), "Project board is not displayed.");
			
			System.out.println("You are on the ' "+getdata("boardName")+"' board page .");
		} 
		catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}
	

}
