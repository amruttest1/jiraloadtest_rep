package com.jiraload.page;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.jiraload.pagefactory.testbase;

public class CreateIssuePage extends testbase {
 WebDriver driver;
 	String Str_Summary= "Created issue from the selenium issue type is Bug ";
 	String Str_Description ="Issue is created from the selenium .";
	
 	public CreateIssuePage(WebDriver driver) {
		this.driver= driver;
	}
	
	public void createIssue()
	{   // Precondition user must loged in 		
		//click on the create button from the menu.
		// switch to the child window 
		// select project 
		//select issue type
		//enter summary 
		// enter reporter
		// enter Description.
		// Submit button 
		// verify the issue created by the entering summary test in search field and search 
		
		try {
			 // select on create issue button
			WebElement btn_create = driver.findElement(By.id("create_link")) ;
			btn_create.click();
			
			//switch to the child window.
			String parent= driver.getWindowHandle();
			  driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);	  

				 for(String Child : driver.getWindowHandles()){
					 driver.switchTo().window(Child);
				 }
				 
				 // Select Project from dropdown
				 
				 WebElement ele_Project= driver.findElement(By.id("project-field"));
				 ele_Project.sendKeys(getdata("projectname"));
				  driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
				  ele_Project.sendKeys(Keys.TAB);
				  //enter the issue type
				  int attempts = 0;
			        while(attempts < 2) {
			            try {
				  WebElement ele_IssueType = driver.findElement(By.id("issuetype-field"));
				  Thread.sleep(1000);
				  ele_IssueType.sendKeys(getdata("BugIssueType"));
				  //ele_IssueType.sendKeys(Keys.TAB);
				  boolean result = true;
	                break;
	            } catch(StaleElementReferenceException e) {
	            }
	            attempts++;
	        }
				  
				  // enter summary 
			        while(attempts < 3) {
			            try {
							  WebElement ele_summary = driver.findElement(By.id("summary"));
							  ele_summary.click();
							  ele_summary.sendKeys(Str_Summary);
							  ele_summary.sendKeys(Keys.TAB);
					  boolean result = true;
		                break;
	            } catch(StaleElementReferenceException e) {
	            }
	            attempts++;
	        }
				  // enter reporter 
				  WebElement ele_Reporter = driver.findElement(By.id("reporter-field"));
				  ele_Reporter.click();
				  ele_Reporter.clear();
				  ele_Reporter.sendKeys(getdata("username"));
				  ele_Reporter.sendKeys(Keys.TAB);
				  // enter description 
				  WebElement ele_Description = driver.findElement(By.id("description"));
				  ele_Description.sendKeys(Str_Description);
				  ele_Description.sendKeys(Keys.TAB);
				  // submit form (Click on create button)
				  WebElement btn_Submit = driver.findElement(By.id("create-issue-submit")) ;
				  btn_Submit.click();
				
				WebElement msg= driver.findElement(By.xpath("//div[@class='aui-message aui-message-success success closeable shadowed aui-will-close']"));
					System.out.println(msg.getText());
				  Assert.assertTrue(msg.isDisplayed(),"Issue create fail..");
				  System.out.println("Issue successfully created ...");
				  
		} catch (Exception e) {
			 System.out.println("Issue creation  fail ...");
			e.printStackTrace();
		}
		
		
		
	}
	
}
