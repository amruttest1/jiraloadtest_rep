package com.jiraload.page;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import com.jiraload.pagefactory.testbase;

public class ViewIssue extends testbase {
	WebDriver driver;
	//webelement variables 
	By Menuissuelink= By.xpath("//*[@id='find_link']");
	By SearchIssue =By.xpath("//*[@id='issues_new_search_link_lnk']");
	By SearchtxtBox =By.xpath("//*[@id='advanced-search']");
	
	public ViewIssue(WebDriver driver) {
		this.driver= driver;
	}
	
	public void viewSpecificIssue() throws FileNotFoundException, IOException
	{	
		try {
			// jql querystring
			String JQLIssuekey="IssueKey= "+getdata("IssueKey");
			//click on issue from menu 
			System.out.println("You are searching for "+getdata("IssueKey")+"  Isuue.");
			
			driver.findElement(Menuissuelink).click();
			//click on sear issue from the issue dropdown
			driver.findElement(SearchIssue).click();			
			//Search issue by using jql statement.
			Assert.assertTrue(driver.findElement(By.xpath("//*[contains(text(),'Advanced')]")).isDisplayed(),"Advance search need to click.");
			driver.findElement(By.xpath("//*[contains(text(),'Advanced')]")).click();
			// enter search text in the searchtext box.
			WebElement ele_advancesearch=driver.findElement(SearchtxtBox);
			ele_advancesearch.sendKeys(JQLIssuekey);
			ele_advancesearch.sendKeys(Keys.ENTER);
			// enter jql query and search the issue 
			driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
			 int attempts = 0;
		        while(attempts < 2) {
		            try {
		            	// select issue from the search result		            	 
		     			WebElement ele_Selectissue= driver.findElement(By.xpath("//a[@class='issue-link'][contains(text(),'"+getdata("IssueKey")+"')]"));
		     			
		     			driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
		     			  //open issue in new tab 
		     			
		     			ele_Selectissue.sendKeys(Keys.CONTROL,Keys.ENTER,Keys.TAB);
		     			System.out.println("Selected issue opened in to new browser tab.");
		     			  boolean result = true;
		                break;
		            } catch(StaleElementReferenceException e) {
		            }
		            attempts++;
		        }
		
		} 
		catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
	}

}
