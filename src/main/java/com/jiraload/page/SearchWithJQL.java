package com.jiraload.page;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.jiraload.pagefactory.testbase;

public class SearchWithJQL extends testbase{
	/*
	 * Search issue with JQL Query
	 * select the issue from the header
	 * click on the search issue link 
	 * look for the advance search button .
	 * enter the JQLquery
	 * click on the search button.	
	 */
		WebDriver driver;
	 
	 	By Menuissuelink= By.xpath("//*[@id='find_link']");
		By SearchIssue =By.xpath("//*[@id='issues_new_search_link_lnk']");
		By SearchtxtBox =By.xpath("//*[@id='advanced-search']");
	 
	 
	public SearchWithJQL( WebDriver driver) {
		this.driver =driver;		
	}
	
	public void searchIssueWithJQL() throws FileNotFoundException, IOException
	{
		// get query from the config.properties file .
		String JQLstring=getdata("SearchJQLQuery");
		
		
		try {
			driver.findElement(Menuissuelink).click();
			//click on sear issue from the issue dropdown
			driver.findElement(SearchIssue).click();			
			//Search issue by using jql statement.
			try {
				Assert.assertFalse(driver.findElement(By.xpath("//*[contains(text(),'Advanced')]")).isDisplayed(),"Advance search ");
				driver.findElement(By.xpath("//*[contains(text(),'Advanced')]")).click();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// enter search text in the searchtext box.
			driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
			
			System.out.println("Query you entered : "+JQLstring);
			
			WebElement ele_advancesearch=driver.findElement(SearchtxtBox);
			driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
			ele_advancesearch.sendKeys(JQLstring);
			ele_advancesearch.sendKeys(Keys.ENTER);
		//	driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
