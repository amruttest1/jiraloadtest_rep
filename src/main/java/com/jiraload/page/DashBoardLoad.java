package com.jiraload.page;

import static org.testng.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.jiraload.pagefactory.testbase;

public class DashBoardLoad extends testbase {
    WebDriver driver;
        
    
    
    public DashBoardLoad( WebDriver driver) {
		this.driver=driver;
	}    
    
    public void dashboardload()
    {
    	 try {
    		 			menuDashBoardClick();
    		 		
    		 		 // click on manage dashboard    		 		 
    		 		 selectDashboardFromManageBoard();   		 		  
    		 		 
    		  
	    		 dashBoardAssertion();
    		   
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
    	
    	
    	
    }

	 public void dashBoardAssertion() throws FileNotFoundException, IOException {
		assertEquals(getdata("DashBoardName"), driver.findElement(By.xpath("//*[@id='dashboard-content']//h1")).getText());
		
		 System.out.println("Dashboard displayed ..");
	}

	public void selectDashboardFromManageBoard() throws FileNotFoundException, IOException {
		
		WebElement manageDashboard= driver.findElement(By.xpath("//*[@id='manage_dash_link_lnk']"));
		 manageDashboard.click();
		 
		 // select dashboard item from the list 
		   WebElement selectItem = driver.findElement(By.xpath("//*[contains(text(),'"+ getdata("DashBoardName")+"')]"));
		  selectItem.click();
	}

	public void menuDashBoardClick() throws InterruptedException {
		WebElement MenuDashboard = driver.findElement(By.xpath("//*[@id='home_link']"));
		MenuDashboard.click();
//		Thread.sleep(500);
		WebElement listDashBoard =driver.findElement(By.xpath("//*[@id='dashboard_link_main']/ul"));
		 Thread.sleep(1000);
		 System.out.println(listDashBoard.toString());
		 System.out.println("********************************************");
		 System.out.println(listDashBoard.getText());
		 System.out.println("********************************************");
	}
	
	

}
