package com.jiraload.page;

import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.jiraload.pagefactory.testbase;

public class LoginLogoutclass extends testbase{
	 WebDriver driver;
	 
	 public LoginLogoutclass(WebDriver driver) {
		this.driver= driver;
	}
	 
	 public void login()
	 {
	 	 try
	 	 {   	
	 		 // click on the login link from the top right corner
	 		 driver.findElement(By.xpath("//*[@class='aui-nav-link login-link']")).click();
	 		 driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	 		// System.out.println(driver.getCurrentUrl());
	 		 WebElement login = driver.findElement(By.id(getdata("login.username")));
			 login.sendKeys(getdata("username"));
			// Thread.sleep(2000);
			 WebElement password = driver.findElement(By.id(getdata("login.password")));
			 password.sendKeys(getdata("password"));
			// Thread.sleep(2000);			
			 WebElement loginbtn = driver.findElement(By.name("login"));
			 loginbtn.click();
			// Thread.sleep(2000);			
			 assertTrue(driver.findElement(By.id("dashboard")).isDisplayed(),"Login successfully .");
 	 }
 	catch(Exception e)
 	{
 		System.out.println(e.toString());
 		
 	}
	 }
	 
	 public void Logout() throws InterruptedException
	 {
		 try{
			 	Thread.sleep(2000);
			 	 WebElement admin = driver.findElement(By.xpath("//*[@id='header-details-user-fullname']/span/span/img"));
			 	driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
			 	 admin.click();
			 	// System.out.println("Click on logout button...");
			 	driver.findElement(By.xpath("//*[@id='log_out']")).sendKeys(Keys.ENTER);
			
			 	}
			 	catch(Exception e)
			 	{
			 		System.out.println("Logout successfully...... ");
			 		
			 	}
			 	Thread.sleep(2000);
			 	
			 //	driver.quit();
	 }
	 
}
