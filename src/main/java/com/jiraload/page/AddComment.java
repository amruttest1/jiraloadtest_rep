package com.jiraload.page;

import static org.testng.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.jiraload.pagefactory.testbase;

public class AddComment extends testbase {
//Login to the jira 
	// select the issue which need to add comment .
// search the issue from the search .
	// click on  the Comment button 
	// add  comment in to the comment textbox .
	 // click on add button .
	// capture the time of comment added. 
	
	  WebDriver driver ;
	public AddComment(WebDriver driver) {
		this.driver= driver;
	}
	
	public void searchissue() throws FileNotFoundException, IOException
	{
		// search issue from the search.
	 WebElement ele_searchTxtBox= driver.findElement(By.id("quickSearchInput"));
	 ele_searchTxtBox.sendKeys(getdata("SearchIssueToAddComment"));
	 ele_searchTxtBox.sendKeys(Keys.ENTER);
	 		
	}
	public  void newAddComment()
	{ String Str_comments ="This is new comment to the issue added from the selenium ";
	 try {
		 	System.out.println("You are seraching for issue : "+getdata("SearchIssueToAddComment"));
		 	
		 	// search issue
		 	searchissue();
		 	WebElement recentComment= driver.findElement(By.xpath("//*[@class='issuePanelWrapper']"));
		 	WebElement ele_time =recentComment.findElement(By.xpath("//*[@ class='mod-content']//div[@class='issuePanelContainer']/div[last()]//*[@class='livestamp']"));
			
		 //	WebElement ele_time1 =driver.findElement(By.xpath("//*[@ class='mod-content']//div[@class='issuePanelContainer']/div[last()]//*[@class='livestamp']"));
			// System.out.println("Last updated at  "+ele_time.getText());
		 	
		 WebElement btn_comments= driver.findElement(By.name("add-comment"));
		 btn_comments.click();
		 
		 WebElement txt_comments= driver.findElement(By.id("comment"));
		 txt_comments.sendKeys(Str_comments);
		 
		 
		 WebElement btn_add= driver.findElement(By.id("issue-comment-add-submit"));
		 btn_add.click();
		 
		 recentComment= driver.findElement(By.xpath("//*[@class='issuePanelWrapper']//*[contains(text(),'"+Str_comments+"')]"));
		// System.out.println(recentComment.toString());
		 assertTrue(recentComment.isDisplayed(), "Comment not added");
		 System.out.println(recentComment.getText());
		 
		// Thread.sleep(2000);
		 
		 ele_time =recentComment.findElement(By.xpath("//*[@ class='mod-content']//div[@class='issuePanelContainer']/div[last()]//*[@class='livestamp']"));
		 System.out.println("Last updated at  "+ele_time.getText());
		 
		// System.out.println("Last updated at  "+System.nanoTime());
		 

	} catch (Exception e) {
		e.printStackTrace();
	}	
	}
	
}
