package com.jiraload.pagefactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;

public class testbase {	
	
	static Properties prop;
	public static WebDriver driver= null;
	public static WebDriverWait wait;
	
	public void inti() throws FileNotFoundException, IOException
	{	
		loadData();		
		firefoxbrowser(getdata("browserName"));
		
	}
		
	public static void loadData() throws IOException,FileNotFoundException
	{
		prop= new Properties();
		File f = new File("src//main//java//com//jiraload//config//config.properties");
		// File f = new File(System.getProperty("user.dir")+"//src//main//java//com//jiraload//config//config.properties");
		//System.out.println(f);
		FileReader obj= new FileReader(f);
		prop.load(obj);
		
		// f = new File(System.getProperty("user.dir")+"//src//main//java//com//jiraload//config//locator.properties");
		 f = new File("src//main//java//com//jiraload//config//locator.properties");
		 obj= new FileReader(f);
		 prop.load(obj);
		
	}
	// code added for linux use 
	public static void loadDataForLinux() throws IOException,FileNotFoundException
{
	prop= new Properties();
	File f = new File(System.getProperty("user.dir")+"/src/main/java/com/jiraload/config/config.properties");
	FileReader obj= new FileReader(f);
	prop.load(obj);
	
}
	public static String getdata(String Data) throws FileNotFoundException, IOException
	{
		loadData();
		String data= prop.getProperty(Data);
		return data;
			
	}
	public static WebDriver getDriver() {
		return driver;
	}
	
	public static WebDriver firefoxbrowser(String Browser) throws FileNotFoundException, IOException
	{	// open  firefox webdriver
		String driverpath =(System.getProperty("user.dir")+"\\geckodriver.exe");
		if(driver == null)
			{
				//System.out.println(driverpath);
				System.setProperty("webdriver.gecko.driver",driverpath);			
				FirefoxOptions options = new FirefoxOptions();
				options.setHeadless(true);		
				driver= new FirefoxDriver(options);
				options.addArguments("window-size=1200x600");
				//Url enter			
				JiraURL();
		}
		return driver;
		 
	}
	// linux headless code 
		public static WebDriver fireFoxbrowserForLinux(String Browser) throws FileNotFoundException, IOException
		{	// open  firefox webdriver

	  		System.setProperty("webdriver.gecko.driver", "/geckodriver");
			FirefoxBinary firefoxBinary = new FirefoxBinary();
			firefoxBinary.addCommandLineOptions("--headless");
			FirefoxOptions firefoxOptions = new FirefoxOptions();
			firefoxOptions.setLogLevel(FirefoxDriverLogLevel.TRACE);
			firefoxOptions.setBinary(firefoxBinary);
			FirefoxDriver driver = new FirefoxDriver(firefoxOptions);
			driver.get(getdata("URL"));
			
			return driver;
			 
		}
	
	public static void JiraURL() {
		driver.get("http://demo.amrutsoftware.com:8080");
	}

	@AfterSuite
	public void closeBrowser()
	{
		driver.quit();
	}
		
}
