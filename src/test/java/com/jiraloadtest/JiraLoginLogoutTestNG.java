package com.jiraloadtest;
import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.jiraload.page.LoginLogoutclass;
import com.jiraload.pagefactory.testbase;

public class JiraLoginLogoutTestNG extends testbase{
	LoginLogoutclass loginlogout;
	
	 @BeforeSuite
		public void setUP() throws IOException{
			inti();
	}
	//login to jira 
	@Test(priority = 1)
	 public void JiraLogin()
	 {	
	 	try {
			loginlogout = new LoginLogoutclass(driver);
			loginlogout.login();
			System.out.println("Login successfully..");
		} 
	 	catch (Exception e) 
	 	{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
// logout from jira
	@AfterTest
	 public void JiraLogout() throws InterruptedException
	 {
		loginlogout = new LoginLogoutclass(driver);
		loginlogout.Logout();
	
	 	
	}
	
	
}
