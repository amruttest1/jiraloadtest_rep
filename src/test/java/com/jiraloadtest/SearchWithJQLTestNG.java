package com.jiraloadtest;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.annotations.Test;

import com.jiraload.page.SearchWithJQL;
import com.jiraload.pagefactory.testbase;

public class SearchWithJQLTestNG  extends testbase{

	SearchWithJQL SWJ;
	
	@Test(priority= 14)
	public void searchissuewithJQLquery() throws FileNotFoundException, IOException
	{
		SWJ= new SearchWithJQL(driver);
		SWJ.searchIssueWithJQL();
	}
}
