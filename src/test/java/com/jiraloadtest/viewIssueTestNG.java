package com.jiraloadtest;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.annotations.Test;

import com.jiraload.page.ViewIssue;
import com.jiraload.pagefactory.testbase;

public class viewIssueTestNG extends testbase {
	ViewIssue vi;
	
	@Test(priority= 12)
	public void OpenIssue() throws FileNotFoundException, IOException
	{
		vi= new ViewIssue(driver);
		vi.viewSpecificIssue();
	}
	 

}
