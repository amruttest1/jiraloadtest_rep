package com.jiraloadtest;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.jiraload.page.DashBoardLoad;
import com.jiraload.pagefactory.testbase;

public class DashBoardTestNG extends testbase{
	DashBoardLoad DBL;
	
	@Test(priority= 8)
	public void adminMenuDashboard() throws InterruptedException {
		DBL= new DashBoardLoad(driver);
		DBL.menuDashBoardClick();
	}
	@Test(priority=9)
	public void selectItemfromManageBoardPage() throws FileNotFoundException, IOException
	{
		DBL.selectDashboardFromManageBoard();
	}
	@Test(priority=10)
	public void Message() throws FileNotFoundException, IOException
	{
		DBL.dashBoardAssertion();
	}
}
