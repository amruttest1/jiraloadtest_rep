package com.jiraloadtest;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.annotations.Test;

import com.jiraload.page.ViewBoard;
import com.jiraload.pagefactory.testbase;

public class viewBoardTestNG extends testbase{

	ViewBoard vb;
	@Test(priority= 11)
	public void projectViewBoard () throws FileNotFoundException, IOException
	{
		vb=new ViewBoard(driver);
		vb.projectBoard();
	}
	
	
}
