package com.jiraloadtest;

import org.testng.annotations.Test;

import com.jiraload.page.CreateIssuePage;
import com.jiraload.pagefactory.testbase;


public class CreateIssueTestNG extends testbase{
	CreateIssuePage CIP;
	
	@Test(priority = 2)
	public void CreateIssue()
	{
		CIP= new CreateIssuePage(driver);
		CIP.createIssue();
	}

}
