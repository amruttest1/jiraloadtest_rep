package com.jiraloadtest;

import java.io.IOException;

import javax.servlet.annotation.WebListener;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.jiraload.page.ProjectDetail;
import com.jiraload.pagefactory.testbase;

public class DefectLoadTestNG  extends testbase{
	
	ProjectDetail PD;
	
//	 @BeforeSuite
//		public void setUP() throws IOException{
//			inti();
//	}
	 @Test(priority = 3)
		public void adminProjectload() {
			PD= new ProjectDetail(driver);
			PD.selectProject();
		}
	 @Test(priority = 4)
	 public void clickIssueMenu()
	 {
		 WebElement MenuIssue = driver.findElement(By.xpath("//*[@id='find_link']"));
		 MenuIssue.click();
		 	 
	 }
	 @Test(priority = 5)
	 public void searchForIssueLinkOfIssueMenu() {
			// click on search issue link from the dropdown 
			//IssurSearchLink.click();
			WebElement IssueSearchLink=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='issues_new_search_link_lnk']")));
			IssueSearchLink.click();
		}
	
	 @Test(priority =6)
		public void bugQuerySearchString() throws InterruptedException {
			// enter the search string	 
		 
		 	String Bugsearch="project =M63_ProjectOne and issuetype = Bug";
			WebElement searchtxtbox=driver.findElement(By.xpath("//*[@id='advanced-search']"));				
			searchtxtbox.sendKeys(Bugsearch);
			searchtxtbox.sendKeys(Keys.ENTER);
			//Thread.sleep(2000);
		}
	 @Test(priority = 7)
		private void clickFirstBugfromSearchResult() {
			WebElement firstissue=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='issuetable']/tbody/tr/td[2]/a")));
			System.out.println("Issue Key : "+firstissue.getAttribute("data-issue-key"));			
			firstissue.click();			
			System.out.println("Clicked ...");
		}
	 
	 
}
